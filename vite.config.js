import { defineConfig } from "vite";
import * as path from "path";
import { svelte } from "@sveltejs/vite-plugin-svelte";

const { IS_DEMO } = process.env;

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],
  server: {
    port: 3010,
  },
  build: {
    ...(IS_DEMO
      ? {}
      : {
          lib: {
            entry: path.resolve(__dirname, "src/Editor.svelte"),
            name: "SuttyEditor",
          },
          rollupOptions: {
            // make sure to externalize deps that shouldn't be bundled
            // into your library
            external: [],
          },
        }),
    sourcemap: true,
  },
});
