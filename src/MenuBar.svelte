<script lang="ts">
  import { TextSelection, type EditorState } from "prosemirror-state";
  import type { EditorView } from "prosemirror-view";
  import type { Command, MarkMatch } from "./ps-utils";

  import { debounceTimer } from "svelte-reactive-debounce";
  import { nanoid } from "nanoid";
  import { lang } from "./i18n_utils";
  import BlockSelect from "./menubar/BlockSelect.svelte";
  import AlignSelect from "./menubar/AlignSelect.svelte";
  import UploadItem from "./menubar/UploadItem.svelte";
  import ListItem from "./menubar/ListItem.svelte";
  import BlockQuoteItem from "./menubar/BlockQuoteItem.svelte";
  import SimpleMarkItem from "./menubar/SimpleMarkItem.svelte";
  import NotaAlPieIcon from "./menubar/nota-al-pie.min.svg?raw";
  import { ListKind } from "./utils";
  import {
    updateMark,
    removeMark,
    markIsActive,
    getFirstMarkInSelection,
    selectMark,
  } from "./ps-utils";
  import { insertPoint } from "prosemirror-transform";
  import { Fragment } from "prosemirror-model";
  import { writable, type Writable } from "svelte/store";

  const locale = lang();
  const locales = {
    link: {
      es: "Enlace externo",
      en: "External link",
    },
    mark: {
      es: "Resaltado",
      en: "Highlight",
    },
    footnote: {
      es: "Nota al pie",
      en: "Footnote",
    },
  };

  interface LangString {
    es: string;
    en: string;
  }
  interface Color {
    name: string;
    id: string;
    label: LangString;
    help: LangString;
    enabled: boolean;
    checkbox?: HTMLInputElement;
  }

  const colors: Color[] = [
    {
      name: "color",
      id: nanoid(),
      label: {
        es: "Color",
        en: "Color",
      },
      help: {
        es: "Habilita este casillero para elegir un color",
        en: "Enable this checkbox to pick a color",
      },
      enabled: true,
    },
    {
      name: "backgroundColor",
      id: nanoid(),
      label: {
        es: "Resaltado",
        en: "Highlight",
      },
      help: {
        es: "Habilita este casillero para elegir un color",
        en: "Enable this checkbox to pick a color",
      },
      enabled: true,
    },
  ];
  const colorCache = {};
  const defaultColors = {
    color: "inherit",
    backgroundColor: "#fcf8e3",
  };

  function getFirstMarkInSelectionAndUpdateColorState(
    state: EditorState,
  ): MarkMatch {
    const match = getFirstMarkInSelection(
      view.state,
      view.state.schema.marks.mark,
    );

    if (match) {
      for (const color of colors) {
        const attrValue = match.mark.attrs[color.name];
        color.enabled = !(attrValue === null || attrValue === "inherit");

        // XXX: Cambiamos esto a mano porque si estamos pasando de un
        // mark a otro el elemento no se recrea, con lo que no cambian los
        // checkboxes.
        if (color.checkbox) color.checkbox.checked = color.enabled;
      }
    }

    return match;
  }

  const timer = debounceTimer(300);
  let editingMark;

  $: if ($timer.up()) {
    state;
    editingMark = getFirstMarkInSelectionAndUpdateColorState(view.state);
  }

  function addToColorCache(id: string, attr: string, color: string) {
    if (!colorCache[id]) colorCache[id] = {};

    colorCache[id][attr] = color;
  }

  function getFromColorCache(
    id: string,
    attr: string,
    defaultValue: string,
  ): string {
    if (!colorCache[id]) return defaultValue;

    return colorCache[id][attr] || defaultValue;
  }

  function runCommand(command: Command) {
    command(view.state, view.dispatch);
  }

  // TODO: https://github.com/ProseMirror/website/blob/master/example/footnote/index.js#L32
  function notaAlPie() {
    if (view.state !== state) return;
    const node = state.schema.nodes.footnote.createChecked(
      { id: nanoid() },
      Fragment.empty,
    );
    const point = insertPoint(
      state.doc,
      state.selection.to,
      state.schema.nodes.footnote,
    );

    view.dispatch(view.state.tr.insert(point, node));
  }

  export let editingLink: Writable<false | "new" | "selection">;

  function toggleEditingLink() {
    const match = getFirstMarkInSelection(
      view.state,
      view.state.schema.marks.link,
    );
    if (match) {
      selectMark(match, view.state, view.dispatch);
      $editingLink = "selection";
    } else if (!view.state.selection.empty) {
      $editingLink = "selection";
    } else {
      $editingLink = "new";
    }
  }

  function toggleEditingMark() {
    const type = view.state.schema.marks.mark;
    const match = getFirstMarkInSelection(view.state, type);

    if (!match) {
      runCommand(updateMark(type, { ...defaultColors }));
      return;
    }

    runCommand(removeMark(type));
  }

  function onChangeMark(event: Event) {
    const type = view.state.schema.marks.mark;
    const input = event.target as HTMLInputElement;
    const attr = input.name;
    const color = input.value;

    if (color) {
      const attrs = { ...editingMark.mark.attrs };

      attrs[attr] = color;
      defaultColors[attr] = color;

      runCommand(updateMark(type, attrs));
    } else {
      runCommand(removeMark(type));
    }
  }

  function onToggleColor(event: Event) {
    const input = event.target as HTMLInputElement;
    const id = `mark-${editingMark.position}`;
    const attr = input.value;
    const attrs = { ...editingMark.mark.attrs };

    if (input.checked) {
      attrs[attr] = getFromColorCache(id, attr, defaultColors[attr]);
    } else {
      addToColorCache(id, attr, attrs[attr]);
      attrs[attr] = "inherit";
      defaultColors[attr] = "inherit";
    }

    runCommand(updateMark(view.state.schema.marks.mark, attrs));
  }

  const alert: Writable<string | null> = writable(null);

  export let state: EditorState;
  export let focusedView: Writable<EditorView>;
  $: view = $focusedView;
</script>

<div
  class="menubar mb-2 position-sticky py-1"
  style="top: 0; background-color: var(--background);"
>
  <div class="d-flex flex-row flex-wrap align-items-center">
    <UploadItem {view} state={view.state || state} />

    <div class="btn-group mx-1">
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.strong}
        faIcon="bold"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.em}
        faIcon="italic"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.underline}
        faIcon="underline"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.strikethrough}
        faIcon="strikethrough"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.sub}
        faIcon="subscript"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.sup}
        faIcon="superscript"
      />
      <SimpleMarkItem
        {view}
        {alert}
        state={view.state || state}
        type={view.state.schema.marks.small}
        faIcon="font"
        small={true}
      />

      <button
        type="button"
        class="btn btn-info"
        title={locales.footnote[locale]}
        on:mousedown|preventDefault={notaAlPie}
      >
        <!-- TODO: class:active={markIsActive(state, view.state.schema.marks.link)} -->
        {@html NotaAlPieIcon}
      </button>

      <button
        type="button"
        class="btn btn-info"
        title={locales.link[locale]}
        class:active={markIsActive(
          view.state || state,
          view.state.schema.marks.link,
        )}
        on:mousedown|preventDefault={toggleEditingLink}
      >
        <i class="fa fa-external-link" />
        <span class="sr-only">{locales.link[locale]}</span>
      </button>

      <button
        type="button"
        class="btn btn-info"
        title={locales.mark[locale]}
        class:active={markIsActive(
          view.state || state,
          view.state.schema.marks.mark,
        )}
        on:mousedown|preventDefault={toggleEditingMark}
      >
        <i class="fa fa-tint" />
        <span class="sr-only">{locales.mark[locale]}</span>
      </button>
    </div>

    <BlockSelect {view} state={view.state || state} />

    <div class="btn-group mx-1 flex-wrap flex-md-nowrap">
      <ListItem {view} state={view.state || state} kind={ListKind.Unordered} />
      <ListItem {view} state={view.state || state} kind={ListKind.Ordered} />
    </div>

    <AlignSelect {view} state={view.state || state} />

    <BlockQuoteItem {view} state={view.state || state} />
  </div>

  {#if $alert}
    <div class="alert alert-info" role="alert">
      {$alert}
      <button
        type="button"
        class="close"
        data-dismiss="alert"
        aria-label="Close"
        on:click|preventDefault={() => ($alert = null)}
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  {/if}

  {#if editingMark}
    <div class="row row-cols-1 row-cols-md-2 mb-2">
      {#each colors as color}
        <div class="col form-group">
          <label for={color.id}>{color.label[locale]}</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <label for="{color.id}-checkbox" class="sr-only">
                  {color.help[locale]}
                </label>

                <input
                  type="checkbox"
                  title={color.help[locale]}
                  bind:checked={color.enabled}
                  bind:this={color.checkbox}
                  value={color.name}
                  on:change|preventDefault={onToggleColor}
                />
              </div>
            </div>

            <input
              type="color"
              id={color.id}
              class="form-control"
              name={color.name}
              disabled={!color.enabled}
              on:change|preventDefault={onChangeMark}
              value={getFromColorCache(
                `mark-${editingMark.position}`,
                color.name,
                editingMark.mark.attrs[color.name],
              )}
            />
          </div>
        </div>
      {/each}
    </div>
  {/if}
</div>

<style>
  .menubar :global(svg) {
    width: 1em;
    height: 1em;
  }
</style>
