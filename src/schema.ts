import { type Attrs, Fragment, Schema } from "prosemirror-model";
import { tableNodes } from "prosemirror-tables";
import hyphenateStyleName from "hyphenate-style-name";
import slugify from "slugify";
import { findFootnoteContent, footnoteIdRegexp } from "./footnotes";
import { nanoid } from "nanoid";

const hex = (x: string) => ("0" + parseInt(x).toString(16)).slice(-2);
// https://stackoverflow.com/a/3627747
// TODO: cambiar por una solución más copada
function rgbToHex(rgb: string): string {
  const matches = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  if (!matches) throw new Error("no pude parsear el rgb()");
  return "#" + hex(matches[1]) + hex(matches[2]) + hex(matches[3]);
}

export type Align = null | "start" | "center" | "end";

export type MultimediaKind = "img" | "video" | "audio" | "iframe";

function getAlign(node: HTMLElement): Align {
  type PossibleAlign = "left" | "right" | "justify" | Align;
  let align: PossibleAlign =
    (node.style.textAlign as PossibleAlign) ||
    (node.getAttribute("data-align") as PossibleAlign) ||
    "start";
  if (
    !(node.style.textAlign || node.getAttribute("data-align")) &&
    node.parentElement.tagName === "DIV" &&
    node.parentElement.dataset.align
  ) {
    // legacy alignment from previous editor
    align = node.parentElement.dataset.align as PossibleAlign;
  }

  switch (align) {
    case "left":
      return "start";
    case "right":
      return "end";
    case "justify":
      return null;
    default:
      if (![null, "center", "start", "end"].includes(align)) {
        console.error("align invalido!", { node, align });
        return null;
      }
      return align as Align;
  }
}

function getHeadingAttrs(level: number): (n: HTMLElement) => Attrs {
  return (n) => ({
    level,
    align: getAlign(n),
    id: n.id,
  });
}

const baseSchema = new Schema({
  nodes: {
    doc: {
      content: "block+",
    },

    paragraph: {
      content: "inline*",
      group: "block",
      attrs: { align: { default: "start" } },
      parseDOM: [
        // ignorar párrafos cuando solo son parientes de multimedia
        // si esto no existe, los multimedia quedan dentro del párrafo y
        // al ser inválidos (no son inline*) se borran
        {
          tag: "p",
          skip: true,
          getAttrs(n) {
            if (
              (n as HTMLElement).children.length === 1 &&
              ["VIDEO", "AUDIO", "IFRAME", "IMG"].includes(
                (n as HTMLElement).children[0].nodeName,
              )
            )
              return null;
            return false;
          },
        },
        {
          tag: "p",
          getAttrs(n) {
            return { align: getAlign(n as HTMLElement) };
          },
        },
      ],
      toDOM(node) {
        return ["p", { style: `text-align: ${node.attrs.align}` }, 0];
      },
    },

    blockquote: {
      content: "block+",
      group: "block",
      parseDOM: [{ tag: "blockquote" }],
      toDOM() {
        return ["blockquote", { class: "blockquote" }, 0];
      },
    },

    horizontal_rule: {
      group: "block",
      parseDOM: [{ tag: "hr" }],
      toDOM() {
        return ["div", ["hr"]];
      },
    },

    heading: {
      attrs: {
        level: { default: 1 },
        align: { default: "start" },
        id: { default: null },
      },
      content: "inline*",
      group: "block",
      defining: true,
      parseDOM: [
        { tag: "h1", getAttrs: getHeadingAttrs(1) },
        { tag: "h2", getAttrs: getHeadingAttrs(2) },
        { tag: "h3", getAttrs: getHeadingAttrs(3) },
        { tag: "h4", getAttrs: getHeadingAttrs(4) },
        { tag: "h5", getAttrs: getHeadingAttrs(5) },
        { tag: "h6", getAttrs: getHeadingAttrs(6) },
      ],
      toDOM(node) {
        const text = node.content?.firstChild?.textContent;
        const attrs = {
          style: `text-align: ${node.attrs.align}`,
          id: text ? slugify(text, { lower: true }) : null,
        };

        return ["h" + node.attrs.level, attrs, 0];
      },
    },

    code_block: {
      content: "text*",
      group: "block",
      code: true,
      defining: true,
      marks: "",
      attrs: { params: { default: "" } },
      parseDOM: [
        {
          tag: "pre",
          preserveWhitespace: "full",
          getAttrs: (node) => ({
            // Estamos segurxs que es un HTMLElement porque esto no es una regla de [style]
            params: (node as HTMLElement).getAttribute("data-params") || "",
          }),
        },
      ],
      toDOM(node) {
        return [
          "pre",
          node.attrs.params ? { "data-params": node.attrs.params } : {},
          ["code", 0],
        ];
      },
    },

    ordered_list: {
      content: "list_item+",
      group: "block",
      attrs: { order: { default: 1 } },
      parseDOM: [
        // Ignorar listas de notas al pie
        {
          tag: "ol",
          ignore: true,
          getAttrs(dom: HTMLElement) {
            for (const item of dom.children) {
              // Es una nota al pie
              if (item.id.match(footnoteIdRegexp)) {
                return null;
              }
              return false;
            }
          },
        },
        {
          tag: "ol",
          getAttrs(dom: HTMLElement) {
            return {
              order: (dom as Element).hasAttribute("start")
                ? +(dom as Element).getAttribute("start")
                : 1,
            };
          },
        },
      ],
      toDOM(node) {
        return node.attrs.order == 1
          ? ["ol", 0]
          : ["ol", { start: node.attrs.order }, 0];
      },
    },
    bullet_list: {
      content: "list_item+",
      group: "block",
      parseDOM: [{ tag: "ul" }],
      toDOM: () => ["ul", 0],
    },

    list_item: {
      content: "paragraph block*",
      defining: true,
      parseDOM: [
        {
          tag: "li",
          ignore: true,
          getAttrs(dom: HTMLElement) {
            if (dom.id.match(footnoteIdRegexp)) {
              return null;
            }
            return false;
          },
        },
        {
          tag: "li",
        },
      ],
      toDOM() {
        return ["li", 0];
      },
    },

    text: {
      group: "inline",
    },

    multimedia: {
      group: "block",
      attrs: {
        src: {},
        kind: {},
        // only for img
        alt: { default: "" },
      },
      content: "text*",
      parseDOM: [
        {
          tag: "figure",
          getAttrs(dom) {
            const child: HTMLElement =
              (dom as Element).querySelector("img") ||
              (dom as Element).querySelector("video") ||
              (dom as Element).querySelector("audio") ||
              (dom as Element).querySelector("iframe");

            if (child instanceof HTMLImageElement) {
              return { src: child.src, kind: "img" };
            } else if (child instanceof HTMLVideoElement) {
              return { src: child.src, kind: "video" };
            } else if (child instanceof HTMLAudioElement) {
              return { src: child.src, kind: "audio" };
            } else if (child instanceof HTMLIFrameElement) {
              return { src: child.src, kind: "iframe" };
            }
            return false;
          },
        },
        {
          tag: "img",
          getAttrs(dom) {
            return {
              src: (dom as HTMLImageElement).src,
              kind: "img",
              alt: (dom as HTMLImageElement).alt,
            };
          },
        },
        {
          tag: "video",
          getAttrs(dom) {
            return { src: (dom as HTMLVideoElement).src, kind: "video" };
          },
          getContent() {
            return Fragment.empty;
          },
        },
        {
          tag: "audio",
          getAttrs(dom) {
            return { src: (dom as HTMLAudioElement).src, kind: "audio" };
          },
          getContent() {
            return Fragment.empty;
          },
        },
        {
          tag: "iframe",
          getAttrs(dom) {
            return { src: (dom as HTMLIFrameElement).src, kind: "iframe" };
          },
        },
      ],
      toDOM(node) {
        const { kind, src } = node.attrs;
        const classs = ["iframe", "audio"].includes(kind)
          ? "w-100"
          : "img-fluid";
        const attrs = {
          src: src,
          class: classs,
          controls: ["video", "audio"].includes(kind),
          alt: (kind === "img" && node.attrs.alt) || undefined,
        };

        return ["figure", [kind, attrs]];
      },
      draggable: true,
    },

    footnote: {
      group: "inline",
      content: "text*",
      inline: true,
      // This makes the view treat the node as a leaf, even though it
      // technically has content
      atom: true,
      toDOM: () => ["footnote"],
      attrs: {
        id: {},
      },
      parseDOM: [
        {
          tag: "footnote",
          getAttrs() {
            return { id: nanoid() };
          },
        },
        {
          tag: "a",
          priority: 100,
          getAttrs(node: HTMLElement) {
            const parsed = findFootnoteContent(node);
            if (!parsed) return false;
            else return { id: parsed.id };
          },
          contentElement(node) {
            return findFootnoteContent(node).elWithContent;
          },
        },
      ],
    },

    hard_break: {
      inline: true,
      group: "inline",
      selectable: false,
      parseDOM: [{ tag: "br" }],
      toDOM() {
        return ["br"];
      },
    },
  },

  marks: {
    em: {
      parseDOM: [{ tag: "i" }, { tag: "em" }, { style: "font-style=italic" }],
      toDOM() {
        return ["em"];
      },
    },

    strong: {
      parseDOM: [
        { tag: "strong" },
        {
          tag: "b",
          getAttrs: (node: HTMLElement) =>
            node.style.fontWeight != "normal" && null,
        },
        {
          style: "font-weight",
          getAttrs: (value: string) =>
            /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null,
        },
      ],
      toDOM() {
        return ["strong"];
      },
    },

    underline: {
      parseDOM: [{ tag: "u" }],
      toDOM() {
        return ["u"];
      },
    },
    strikethrough: {
      parseDOM: [{ tag: "del" }],
      toDOM() {
        return ["del"];
      },
    },
    sub: {
      parseDOM: [{ tag: "sub" }],
      toDOM() {
        return ["sub"];
      },
    },
    sup: {
      parseDOM: [{ tag: "sup" }],
      toDOM() {
        return ["sup"];
      },
    },
    small: {
      parseDOM: [{ tag: "small" }],
      toDOM() {
        return ["small"];
      },
    },

    mark: {
      attrs: {
        color: { default: null },
        backgroundColor: { default: null },
        outline: { default: null },
      },
      parseDOM: [
        {
          tag: "mark",
          getAttrs(dom) {
            const attrs = {};
            const domEl = dom as HTMLElement;

            for (const attr of ["color", "backgroundColor"]) {
              const value = domEl.style[attr];

              if (!value) continue;

              attrs[attr] = value === "inherit" ? value : rgbToHex(value);
            }

            return attrs;
          },
        },
      ],
      toDOM(node) {
        const colors = [];
        const attrs = { ...node.attrs };

        if (Object.values(attrs).every((x) => x === null || x === "inherit")) {
          attrs.outline = "1px solid pink";
        }

        for (const attr in attrs) {
          let color = attrs[attr];

          if (!color) continue;

          colors.push(`${hyphenateStyleName(attr)}:${color}`);
        }

        return ["mark", { style: colors.join(";") }];
      },
    },

    link: {
      attrs: {
        href: {},
      },
      inclusive: false,
      parseDOM: [
        {
          tag: "a[href]",
          getAttrs(dom) {
            return {
              href: (dom as Element).getAttribute("href"),
            };
          },
        },
      ],
      toDOM(node) {
        const attrs = {
          ...node.attrs,
          rel: "noopener",
          target: "_blank",
          referrerpolicy: "strict-origin-when-cross-origin",
        };

        return ["a", attrs];
      },
    },

    code: {
      parseDOM: [{ tag: "code" }],
      toDOM() {
        return ["code"];
      },
    },
  },
});

export const schema = new Schema({
  marks: baseSchema.spec.marks,
  nodes: baseSchema.spec.nodes.append(
    tableNodes({
      tableGroup: "block",
      cellContent: "block+",
      cellAttributes: {},
    }),
  ),
});
