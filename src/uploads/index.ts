import { EditorState, Plugin } from "prosemirror-state";
import { Decoration, DecorationSet } from "prosemirror-view";
import * as ActiveStorage from "@rails/activestorage";
import { h } from "../utils";
import type { MultimediaKind } from "../schema";

export const activeStorageEndpoint = import.meta.env.DEV
  ? `blob:`
  : `${origin}/rails/active_storage`;

function updatePlaceholderProgress(id: string, progressN: number) {
  const el = document.querySelector<HTMLElement>(
    `.sutty-editor-placeholder-${id} .progress-bar`
  );
  if (!el) return;
  const progress = "" + progressN * 100;
  el.style.width = `${progress}%`;
  el.setAttribute("aria-value-now", progress);
}

export function uploadFile(
  file: File,
  placeholderIdToUpdate: string
): Promise<{ url: string; contentType: string }> {
  return new Promise((resolve, reject) => {
    if (import.meta.env.DEV) {
      if (file.type === "sutty/download-from-url") {
        // shimmear el funcionamiento especial del backend, falla si no tiene CORS el sitio
        file
          .text()
          .then((url) => fetch(url))
          .then((res) => res.blob())
          .then((blob) =>
            uploadFile(
              new File([blob], "-", {
                type: blob.type,
              }),
              placeholderIdToUpdate
            )
          )
          .then((url) => resolve(url))
          .catch((error) => reject(error));
        return;
      }

      let run = 0;
      const interval = setInterval(() => {
        run++;
        const limit = 15;
        updatePlaceholderProgress(placeholderIdToUpdate, run / limit);
        if (run < limit) return;
        if ((window as any).SUTTY_EDITOR_DEV_UPLOAD_FAIL) {
          reject(new Error("kekw"));
        } else {
          resolve({ url: URL.createObjectURL(file), contentType: file.type });
        }
        clearInterval(interval);
      }, 100);
    } else {
      const upload = new ActiveStorage.DirectUpload(
        file,
        `${activeStorageEndpoint}/direct_uploads`,
        {
          directUploadWillStoreFileWithXHR(request) {
            request.upload.addEventListener("progress", (event) => {
              updatePlaceholderProgress(
                placeholderIdToUpdate,
                event.loaded / event.total
              );
            });
          },
        }
      );

      upload.create(async (error: any, blob: any) => {
        if (error) {
          reject(error);
        } else {
          const url = `${activeStorageEndpoint}/blobs/${blob.signed_id}/${blob.filename}`;
          try {
            const res = await fetch(url, { method: "HEAD" });

            resolve({ url, contentType: res.headers.get("content-type") });
          } catch (error) {
            reject(error);
          }
        }
      });
    }
  });
}

export type Action =
  | {
      add: {
        id: string;
        unknownProgress: boolean;
        pos: number;
      };
    }
  | {
      error: { id: string; message: string };
    }
  | {
      remove: { id: string };
    };

import { lang } from "../i18n_utils";
const locales = {
  uploadingFile: {
    es: "Subiendo archivo...",
    en: "Uploading file...",
  },
  errorDownloadingImage: {
    es: "Hubo un error descargando esta imagen. Si la copiaste y pegaste desde otra página o documento, intentá descargarla y subirla manualmente. ",
    en: "There was an problem downloading this image. If you copied it from another webpage or document, try downloading and uploading it manually. ",
  },
};

export const placeholderPlugin = new Plugin({
  state: {
    init(): DecorationSet {
      return DecorationSet.empty;
    },
    apply(tr, set: DecorationSet) {
      // Adjust decoration positions to changes made by the transaction
      set = set.map(tr.mapping, tr.doc);
      // See if the transaction adds or removes any placeholders
      let actions: Array<Action> = tr.getMeta(this);
      if (actions) {
        for (const action of actions) {
          if ("add" in action) {
            let widgetEl = h(
              "div",
              {
                class: `ProseMirror-placeholder sutty-editor-placeholder-${action.add.id}`,
              },
              [
                locales.uploadingFile[lang()],
                h("div", { class: "progress" }, [
                  h(
                    "div",
                    {
                      class: `progress-bar ${
                        action.add.unknownProgress
                          ? "progress-bar-striped progress-bar-animated"
                          : ""
                      }`,
                      attributes: {
                        role: "progressbar",
                        "aria-valuenow": "0",
                        "aria-valuemin": "0",
                        "aria-valuemax": "100",
                      },
                      style: { width: "0%" },
                    },
                    []
                  ),
                ]),
              ]
            );
            let deco = Decoration.widget(action.add.pos, widgetEl, {
              id: action.add.id,
            });
            set = set.add(tr.doc, [deco]);
          } else if ("error" in action) {
            const { id } = action.error;
            const matches = set.find(
              undefined,
              undefined,
              (spec) => spec.id == action.error.id
            );
            if (!matches[0]) return set;
            const match = matches[0];

            let widgetEl = h(
              "div",
              {
                class: `ProseMirror-placeholder sutty-editor-placeholder-${id}`,
              },
              [
                h(
                  "div",
                  {
                    class: "alert alert-danger",
                    attributes: { role: "alert" },
                  },
                  [locales.errorDownloadingImage[lang()], action.error.message]
                ),
              ]
            );
            let deco = Decoration.widget(match.from, widgetEl, { id });

            set = set.remove([match]).add(tr.doc, [deco]);
          } else if ("remove" in action) {
            set = set.remove(
              set.find(
                undefined,
                undefined,
                (spec) => spec.id == action.remove.id
              )
            );
          }
        }
      }
      return set;
    },
  },
  props: {
    decorations(state) {
      return this.getState(state);
    },
  },
});

function findPlaceholderDecoration(
  state: EditorState,
  id: string
): Decoration | null {
  const decos: DecorationSet = placeholderPlugin.getState(state);
  const found = decos.find(undefined, undefined, (spec) => spec.id == id);
  return found.length ? found[0] : null;
}

export function findPlaceholder(state: EditorState, id: string): number | null {
  return findPlaceholderDecoration(state, id)?.from;
}

export function mimeToKind(mime: string): MultimediaKind {
  if (mime.match(/^image\/.+$/)) {
    return "img";
  } else if (mime.match(/^video\/.+$/)) {
    return "video";
  } else if (mime.match(/^audio\/.+$/)) {
    return "audio";
  } else if (mime.match(/^application\/pdf$/)) {
    return "iframe";
  } else {
    // TODO: chequear si el archivo es válido antes de subir
    throw new Error("Tipo de archivo no reconocido");
  }
}
