import { Plugin, Transaction } from "prosemirror-state";
import type { Node as ProsemirrorNode } from "prosemirror-model";
import { insertPoint } from "prosemirror-transform";
import { nanoid } from "nanoid";
import {
  activeStorageEndpoint,
  findPlaceholder,
  mimeToKind,
  placeholderPlugin,
  uploadFile,
  type Action,
} from ".";
import { EditorView } from "prosemirror-view";

function reuploadMultimedia(
  tr: Transaction,
  view: EditorView,
  node: ProsemirrorNode,
  pos: number
): [Transaction, any] {
  const { src } = node.attrs;
  // TODO: ¿que hacemos con las blobs?
  const point = insertPoint(tr.doc, pos, view.state.schema.nodes.multimedia);
  const id = nanoid();

  uploadFile(
    new File([src], nanoid() + src.slice(src.lastIndexOf("/") + 1), {
      type: "sutty/download-from-url",
    }),
    id
  )
    .then(async ({ url, contentType }) => {
      const placeholderPos = findPlaceholder(view.state, id);
      // si se borró el placeholder, no subir imágen
      if (placeholderPos == null) return;

      const node = view.state.schema.nodes.multimedia.createChecked({
        src: url,
        kind: mimeToKind(contentType),
      });

      view.dispatch(
        view.state.tr
          .replaceWith(placeholderPos, placeholderPos, node)
          .setMeta(placeholderPlugin, [{ remove: { id } }])
      );
    })
    .catch((err) => {
      console.error("Error resubiendo archivo:", err);
      const action: Action = {
        error: { id, message: err.toString() },
      };
      view.dispatch(view.state.tr.setMeta(placeholderPlugin, [action]));
    });

  return [
    tr,
    {
      add: { id, pos: point, unknownProgress: true },
    },
  ];
}

// Sube los multimedia que no están todavía en ActiveStorage.
export const autoUploadPlugin = new Plugin({
  view() {
    return {
      update(view) {
        let tr = view.state.tr;
        let placeholderActions: any[] = [];

        while (true) {
          let checked = false;
          tr.doc.descendants((node, pos) => {
            if (checked) return false;
            if (node.type.name === "multimedia") {
              if (!node.attrs.src.startsWith(activeStorageEndpoint)) {
                tr = tr.delete(pos, pos + 1);
                const [tra, action] = reuploadMultimedia(tr, view, node, pos);
                tr = tra;
                placeholderActions.push(action);
                checked = true;
                return false;
              }
            }
          });
          if (!checked) break;
        }

        if (tr.docChanged || placeholderActions.length > 0)
          view.dispatch(tr.setMeta(placeholderPlugin, placeholderActions));
      },
    };
  },
});
