import { find } from "linkifyjs";
import { Fragment, Slice, type MarkType, Node } from "prosemirror-model";
import { Plugin, PluginKey } from "prosemirror-state";

export function autolink(): Plugin {
  return new Plugin({
    key: new PluginKey("autolink"),
    props: {
      transformPasted(slice, view) {
        const { state } = view;
        const { selection } = state;

        // Do not proceed if in code block.
        if (state.doc.resolve(selection.from).parent.type.spec.code) {
          return slice;
        }

        let isAlreadyLink = false;
        slice.content.descendants((node) => {
          if (node.marks.some((mark) => mark.type.name === "link")) {
            isAlreadyLink = true;
          }
        });
        if (isAlreadyLink) return slice;

        let textContent = "";
        slice.content.forEach((node) => {
          textContent += node.textContent;
        });

        const link = find(textContent).find(
          (item) =>
            item.isLink && item.value === textContent && item.start === 0
        );
        if (!link) return slice;

        const mark = state.schema.marks.link.create({ href: link.href });
        return new Slice(
          Fragment.from(state.schema.text(link.href, [mark])),
          slice.openStart,
          slice.openEnd
        );
      },
    },
  });
}
