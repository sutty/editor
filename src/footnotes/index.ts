export function findFootnoteContent(
  link: Node
): { id: string; elWithContent: HTMLElement } | null {
  try {
    if (!(link instanceof HTMLAnchorElement)) return null;
    let parent = link.parentElement;
    while (parent.parentElement) parent = parent.parentElement;

    const url = new URL(link.href);
    const matches = url.hash.slice(1).match(footnoteIdRegexp);
    if (!matches || matches.length < 2) return null;
    const id = matches[1];

    let footnoteClone;
    {
      const originalFootnoteEl = parent.querySelector<HTMLElement>(url.hash);
      if (!originalFootnoteEl) return null;
      footnoteClone = originalFootnoteEl.cloneNode(true);
      if (!(footnoteClone instanceof HTMLElement))
        throw new Error("footnote no es HTMLElement");
    }

    // Borrar backlink, no es utilizado en el editor y es regenerado en Editor.svelte
    const backLink = footnoteClone.querySelector(`a[href="#fnef${id}"]`);
    backLink?.remove();

    return { id, elWithContent: footnoteClone };
  } catch {
    return null;
  }
}

export const footnoteIdRegexp = /^fn([A-Za-z0-9_\-]{1,22})$/;
