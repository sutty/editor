import type { Node } from "prosemirror-model";
import { Plugin } from "prosemirror-state";
import { DecorationSet } from "prosemirror-view";

export function findFootnoteIndex(doc: Node, pos: number): number | null {
  let actualIndex: number | null = null;
  let index = 0;
  doc.descendants((childNode, childNodePos) => {
    if (actualIndex !== null) return false;
    if (childNode.type.name === "footnote") {
      if (childNodePos === pos) {
        actualIndex = index;
        return false;
      }
      index++;
    }
  });
  return actualIndex;
}

export const footnoteLinksPlugin = (editorId: string) =>
  new Plugin({
    props: {
      handleClickOn(view, _, node, nodePos, event, direct) {
        if (!direct) return;
        if (node.type.name !== "footnote") return;

        const index = findFootnoteIndex(view.state.doc, nodePos);
        if (index === null) {
          console.error(
            "Clickearon",
            node,
            "pero no pude encontrar la footnote en el documento"
          );
          return false;
        }

        const footnoteContent: HTMLElement = document.querySelector(
          `#sutty-editor-${editorId}-footnote-${index} .ProseMirror`
        );
        if (!footnoteContent) {
          console.error(
            "Clickearon",
            node,
            "que es la footnote con index",
            index,
            "pero no pude encontrar el contenido de la footnote en el documento"
          );
          return false;
        }

        footnoteContent.focus();
        footnoteContent.scrollIntoView({ behavior: "smooth", block: "center" });

        return true;
      },
    },
  });
