// El panel solo soporta estos idiomas
export type Lang = "es" | "en";

export function lang(): Lang {
  const docLang = document.body.parentElement?.lang;
  if (docLang && (docLang == "es" || docLang == "en")) return docLang;
  if (docLang) {
    // la lógica es que si hay un idioma que no conocemos, es más probable que
    // lx usuarix sepa inglés en vez de castellano. ¡colonialismo codificado!
    // de todas maneras es un bug que el panel nos tire un idioma que no conocemos
    console.error("docLang no es ni en ni es! Seteando `en` de prepo...");
    return "en";
  }
  // el panel no tiene lang o estamos fuera del panel
  console.error("No hay docLang! Seteando `es` de prepo...");
  return "es";
}

export function dir(): string {
  return document.body.parentElement?.dir || "ltr";
}
