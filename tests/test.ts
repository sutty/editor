export function test(name: string, testCase: () => void): void {
  try {
    testCase();
  } catch (error) {
    console.log(`not ok ${name}`);
    console.error(error);
  }
  console.log(`ok ${name}`);
}
