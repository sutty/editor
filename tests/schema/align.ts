import { test } from "../test.js";
import { parseHTML } from "./common.js";
import assert from "node:assert/strict";

test("data-align headers", () => {
  // observatoriociudad.org/_es/2021-07-22-avanza-en-la-legislatura-el-proyecto-costa-urbana-de-irsa-incumpliendo-la-convocatoria-a-audiencia-pública.markdown
  const doc =
    parseHTML(`<div data-align="center" style="text-align:center;"><h1>
<strong>💰</strong><strong> Avanza em la Legislatura el proyecto "Costa Urbana" de IRSA incumpliendo la convocatoria a audiencia
pública.</strong>
</h1></div>`);
  assert.equal(doc.content.childCount, 1);
  const h1 = doc.content.child(0);
  assert.equal(h1.type.name, "heading");
  assert.equal(h1.attrs.level, 1);
  assert.equal(h1.attrs.align, "center");
});

test("data-align paragraphs", () => {
  // observatoriociudad.org/_es/2021-11-09-para-el-gcba-el-verde-sólo-debe-florecer-donde-genere-más-dólares.markdown
  const doc = parseHTML(
    `<div data-align="right" style="text-align:right;"><p> <em>Por
Jonatan Baldiviezo, María Eva Koutsovitis, Alejandro Volkind y Myriam Godoy
Arroyo.</em></p></div>`
  );
  assert.equal(doc.content.childCount, 1);
  const paragraph = doc.content.child(0);
  assert.equal(paragraph.type.name, "paragraph");
  assert.equal(paragraph.attrs.align, "end");
});
