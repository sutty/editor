import { JSDOM } from "jsdom";
import { schema } from "../../src/schema.js";
import { DOMParser, Node } from "prosemirror-model";

export function parseHTML(html: string): Node {
  const dom = new JSDOM(html);
  const domParser = DOMParser.fromSchema(schema);
  const doc = domParser.parse(dom.window.document.body);
  return doc;
}
