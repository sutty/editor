# @suttyweb/editor

El editor utilizado en el [panel de Sutty](https://panel.sutty.nl).

## Probar

requerís [pnpm](https://pnpm.io).

```shell
git clone git@0xacab.org:sutty/editor
cd editor
pnpm install
pnpm run dev
# ir a la dirección que te diga Vite
```

por favor corré prettier antes de commitear: `pnpm run format`

## Estructura del código

- el editor en si:
  - `src/Editor.svelte` es el componente a usar.
  - `src/MenuBar.svelte` es el menú que aparece en la parte de arriba del editor.
    - `src/menubar/*.svelte` son los botones que aparecen en el menú.
  - `src/editor.css` es el CSS del editor. ojo que otras partes están _dentro_ de los componentes de Svelte.
  - `src/ps-utils.ts` son funciones útiles para ProseMirror. varias robadas de tiptap v1.
  - `src/schema.ts` es el schema del ProseMirror.
- la demo:
  - `index.html` es el HTML de la demo.
  - `src/demo.css` es el CSS de la demo.
  - `src/main.ts` es el JS de la demo.

## Referencias

- [tiptap](https://github.com/ueberdosis/tiptap)
- El editor de GitLab (basado en tiptap)
  - [Doc interna](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/fe_guide/content_editor.md)
  - [Código](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/app/assets/javascripts/content_editor)
